package service;

import domain.Account;
import domain.Customer;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import repository.CustomerRepository;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
@Primary
public class CustomerServiceImpl implements CustomerService {
    private CustomerRepository customerRepository;

    @Override
    public void save(Customer customer) {
        Customer customerByNationalCode = getByNationalCode(customer.getNationalCode());
        if (customerByNationalCode != null) {
            throw new CustomerWithSameNationalCodeExistedException("This customer with this national code is saved before");
        }
        customerRepository.save(customer);
    }


    @Override
    public Customer getById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("This customer does not exist");
        }
        return customerRepository.getById(id);
    }

    @Override
    public Customer getByName(String name) {

        return customerRepository.getByName(name);
    }

    @Override
    public void delete(Long id) {
        customerRepository.delete(id);
    }

    @Override
    public void update(Long id, Customer newCustomer) {
        if ((id == null) && (newCustomer == null)) {
            throw new IllegalArgumentException("These parameters can not be null");
        }
        customerRepository.update(id, newCustomer);
    }

    @Override
    public List<Customer> getAllCustomers() {
        return customerRepository.getAllCustomers();
    }

    @Override
    public Customer getByNationalCode(String nationalCode) {
        if (nationalCode.length() < 10) {
            throw new IllegalArgumentException("nationalCode must not less than ten");
        }
        return customerRepository.getByNationalCode(nationalCode);
    }

    @Override
    public List<Account> getAllAccounts(long id) {

        return customerRepository.getAllAccounts(id);
    }

    /*
        1- accept one parameter  -> id:long
        2- read by id and find a customer --> customer:Customer
        3- read accounts of customer -- > accounts:List<Account>
        4- create a local variable with 0 value -->sum:BigDecimal
        5- loop in the accounts of this customer
           5-1 get an account of this customer--> account:Account
           5-2 read balance of account -->balance:BigDecimal
           5-3 sum = balance + sum
        6- return sum;
    */
    @Override
    public BigDecimal findSumTotalBalance(long id) {
        Customer customer = getById(id);
        List<Account> accounts = customer.getAccounts();

        BigDecimal sum = BigDecimal.valueOf(0);

        for (int i = 0; i < accounts.size(); i++) {
            Account account = accounts.get(i);
            BigDecimal balance = account.getBalance();
                sum = balance.add(sum);
            }
        return sum;
    }

        public void setCustomerRepository (CustomerRepository customerRepository){
            this.customerRepository = customerRepository;
        }

        @Override
        public Customer getByCustomer (Customer customer){
            return null;
        }
    }
