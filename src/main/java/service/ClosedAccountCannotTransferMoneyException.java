package service;

public class ClosedAccountCannotTransferMoneyException extends RuntimeException {
    public ClosedAccountCannotTransferMoneyException() {
        super();
    }

    public ClosedAccountCannotTransferMoneyException(String message) {
        super(message);
    }

    public ClosedAccountCannotTransferMoneyException(String message, Throwable cause) {
        super(message, cause);
    }

    public ClosedAccountCannotTransferMoneyException(Throwable cause) {
        super(cause);
    }

    protected ClosedAccountCannotTransferMoneyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
