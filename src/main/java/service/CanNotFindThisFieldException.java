package service;

public class CanNotFindThisFieldException extends RuntimeException {
    public CanNotFindThisFieldException() {
        super();
    }

    public CanNotFindThisFieldException(String mesage) {
        super(mesage);
    }

    public CanNotFindThisFieldException(String message, Throwable cause) {
        super(message, cause);
    }

    public CanNotFindThisFieldException(Throwable cause) {
        super(cause);
    }

    public CanNotFindThisFieldException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
