package service;

import domain.Branch;

import java.util.List;

public interface BranchService {
    void save(Branch branch);

    Branch getById(Long id);

    void delete(Long id);

    void update(Long id, Branch newBranch);

    List<Branch> getAllBranch();

}
