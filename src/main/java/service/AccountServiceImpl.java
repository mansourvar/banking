package service;

import domain.Account;
import domain.AccountStatus;
import domain.AccountType;
import repository.AccountRepository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class AccountServiceImpl implements AccountService {
    private AccountRepository accountRepository;

    @Override
    public void save(Account account) {
        if (account.getCustomer() == null) {
            throw new AccountWithoutCustomerException("Account without Customer cannot be saved");
        }
        accountRepository.save(account);
    }


    @Override
    public Account getById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("This account does not exist");
        }
        return accountRepository.getById(id);
    }

    @Override
    public void delete(Long id) {
        accountRepository.delete(id);
    }

    @Override
    public void update(Long id, Account newAccount) {
        if ((id == null) && (newAccount == null)) {
            throw new IllegalArgumentException("These parameters cannot be null");
        }
        accountRepository.update(id, newAccount);
    }

    @Override
    public List<Account> getAllAccounts() {
        return accountRepository.getAllAccounts();
    }

    @Override
    /*
    1- accept two parameters --> id:long , amount:BigDecimal
    2-get by id --> account:Account
    3-read balance of account --> balance:BigDecimal
    4- read AccountType of Account --> accountType:AccountType
    5-get minBalance -->minBalance:BigDecimal
    6-if balance - amount is  less than minBalance
      6-1 throws an Exception.
    7- otherwise balance - amount
    * */
    public void withdraw(long id, BigDecimal amount) {
        Account account = getById(id);
        BigDecimal balance = account.getBalance();
        AccountType accountType = account.getAccountType();
        BigDecimal minBalance = accountType.getMinBalance();

        if (balance.subtract(amount).compareTo(minBalance) == -1) {
            throw new BalanceIsLessThanThresholdException("balance less than thresh old");
        }
        BigDecimal finalBalance = balance.subtract(amount);
        account.setBalance(finalBalance);
        accountRepository.update(id, account);
    }
    /*
     * 1- accept two parameters id:long , amount:BigDecimal
     * 2- read id --> account:Account
     * 3- read balance of account --> balance:BigDecimal
     * 4- balance + amount
     * 5- update account
     * */

    @Override
    public void deposit(long id, BigDecimal amount) {
        Account account = getById(id);
        BigDecimal balance = account.getBalance();
        BigDecimal finalBalance = balance.add(amount);
        account.setBalance(finalBalance);
        update(account.getId(), account);
    }

    /*
     1- Accept three parameters  -- > (sourceAccount,destinationAccount,amount):BigDecimal
     2-get by id --> sourceAccount:Account
     3-get by id --> destinationAccount:Account
     4-read the balance of sourceAccount --> sourceBalance:bigDecimal
     5-read the typeAccount sourceAccount --> sourceAccountType:BigDecimal
     6-read the MinBalance of sourceAccount -- > sourceMinBalance : BigDecimal
     7-transfer amount from sourceAccount to destinationAccount
     8-if after transferring the minBalance of sourceAccount is less than limited
       8-1 throw an exception and avoid transfer
     9- otherwise transfer money.

    * */
    @Override
    public void transfer(long sourceId, long destinationId, BigDecimal amount) {
        Account sourceAccountId = getById(sourceId);
        BigDecimal sourceBalance = sourceAccountId.getBalance();
        AccountType sourceAccountType = sourceAccountId.getAccountType();
        BigDecimal sourceMinBalance = sourceAccountType.getMinBalance();
        Date sourceCloseDate = sourceAccountId.getCloseDate();
        Account destinationAccountId = getById(destinationId);
        BigDecimal destinationBalance = destinationAccountId.getBalance();
        Date destinationCloseDate = destinationAccountId.getCloseDate();
        if (sourceCloseDate != null) {
            throw new TransferringFromClosedAccountIsImpossibleException("Transfer money from closed account is impossible");
        }
        if (destinationCloseDate != null) {
            throw new TransferringToCloseAccountIsImpossibleException("Transfer money to closed account is impossible");
        }


        if (sourceBalance.subtract(amount).compareTo(sourceMinBalance) == -1) {
            throw new BalanceIsLessThanThresholdException("The transfer amount requested is higher than the limit.");
        }
        BigDecimal subtract = sourceBalance.subtract(amount);
        BigDecimal add = destinationBalance.add(amount);
        accountRepository.update(sourceId, destinationAccountId);
    }

    /*
    1- accept one parameter -- > id: long
    2- read base on id --> account:Account
    3- read balance of account --> balance : BigDecimal
    3-withdraw the account with balance
    4-set property closeDate with currentDate
    */
    @Override
    public void close(long id) {
        Account account = getById(id);
        account.setBalance(BigDecimal.valueOf(0));
        account.setCloseDate(new Date());
        accountRepository.update(id, account);
    }


    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }
}
