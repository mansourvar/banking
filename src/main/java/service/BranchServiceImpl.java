package service;

import domain.Branch;
import org.springframework.beans.factory.annotation.Autowired;
import repository.BranchRepository;
import repository.BranchRepositoryImpl;

import java.util.List;

public class BranchServiceImpl implements BranchService {
    private BranchRepository branchRepository;

    @Override
    public void save(Branch branch) {
        branchRepository.save(branch);
    }

    @Override
    public Branch getById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("This branch does not exist");
        }
        return branchRepository.getById(id);
    }

    @Override
    public void delete(Long id) {
        branchRepository.delete(id);
    }

    @Override
    public void update(Long id, Branch newBranch) {
        if ((id == null) && (newBranch == null)) {
            throw new IllegalArgumentException("These parameters can not be null");
        }
        branchRepository.update(id, newBranch);
    }

    @Override
    public List<Branch> getAllBranch() {
        return branchRepository.getAllBranches();
    }

    public void setBranchRepository(BranchRepository branchRepository) {
        this.branchRepository = branchRepository;
    }

}
