package service;

import domain.Account;
import domain.Customer;

import java.math.BigDecimal;
import java.util.List;

public interface CustomerService {
    void save(Customer customer);

    Customer getById(Long id);

    Customer getByName(String name);

    void delete(Long id);

    void update(Long id, Customer newCustomer);

    List<Customer> getAllCustomers();

    Customer getByNationalCode(String nationalCode);

    List<Account> getAllAccounts(long id);

    BigDecimal findSumTotalBalance(long id);

    Customer getByCustomer(Customer customer);
}
