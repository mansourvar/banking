package service;

public class CustomerWithSameNationalCodeExistedException extends RuntimeException{
    public CustomerWithSameNationalCodeExistedException() {
        super();
    }

    public CustomerWithSameNationalCodeExistedException(String message) {
        super(message);
    }

    public CustomerWithSameNationalCodeExistedException(String message, Throwable cause) {
        super(message, cause);
    }

    public CustomerWithSameNationalCodeExistedException(Throwable cause) {
        super(cause);
    }

    protected CustomerWithSameNationalCodeExistedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
