package service;

import domain.Bank;

import java.util.ArrayList;
import java.util.List;

public interface BankService {
    void save(Bank bank);

    Bank getById(Long id);

    void delete(Long id);

    void update(Long id, Bank newBank);

    List<Bank> getAllBanks();
}
