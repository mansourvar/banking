package service;

public class BalanceIsLessThanThresholdException extends RuntimeException {
    public BalanceIsLessThanThresholdException(String message) {
        super(message);
    }

    public BalanceIsLessThanThresholdException(String message, Throwable cause) {
        super(message, cause);
    }

    public BalanceIsLessThanThresholdException(Throwable cause) {
        super(cause);
    }

    protected BalanceIsLessThanThresholdException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
