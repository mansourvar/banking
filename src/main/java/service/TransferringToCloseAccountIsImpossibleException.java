package service;

public class TransferringToCloseAccountIsImpossibleException extends RuntimeException {
    public TransferringToCloseAccountIsImpossibleException() {
        super();
    }

    public TransferringToCloseAccountIsImpossibleException(String message) {
        super(message);
    }

    public TransferringToCloseAccountIsImpossibleException(String message, Throwable cause) {
        super(message, cause);
    }

    public TransferringToCloseAccountIsImpossibleException(Throwable cause) {
        super(cause);
    }

    protected TransferringToCloseAccountIsImpossibleException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
