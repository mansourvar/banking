package service;

import domain.Account;
import domain.Customer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public interface AccountService {
    void save(Account account);

    Account getById(Long id);

    void delete(Long id);

    void update(Long id, Account newAccount);

    List<Account> getAllAccounts();

    void withdraw(long id, BigDecimal amount);

    void deposit(long id,BigDecimal amount);

    void transfer (long sourceId,long destinationId,BigDecimal amount);

    void close (long id);
}
