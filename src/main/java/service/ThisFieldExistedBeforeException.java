package service;

public class ThisFieldExistedBeforeException extends RuntimeException {

    public ThisFieldExistedBeforeException() {
        super();
    }

    public ThisFieldExistedBeforeException(String mesage) {
        super(mesage);
    }
    public ThisFieldExistedBeforeException(String message, Throwable cause) {
        super(message, cause);
    }
    public ThisFieldExistedBeforeException(Throwable cause) {
        super(cause);
    }
    public ThisFieldExistedBeforeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}