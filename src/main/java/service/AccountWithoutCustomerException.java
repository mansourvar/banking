package service;

public class AccountWithoutCustomerException extends RuntimeException {
    public AccountWithoutCustomerException() {
        super();
    }

    public AccountWithoutCustomerException(String message) {
        super(message);
    }

    public AccountWithoutCustomerException(String message, Throwable cause) {
        super(message, cause);
    }

    public AccountWithoutCustomerException(Throwable cause) {
        super(cause);
    }

    protected AccountWithoutCustomerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

