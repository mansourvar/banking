package service;

import domain.Bank;
import repository.BankRepository;
import repository.BankRepositoryImpl;

import java.util.List;

public class BankServiceImpl implements BankService {
    private BankRepository bankRepository;

    @Override
    public void save(Bank bank) {
        bankRepository.save(bank);
    }

    @Override
    public Bank getById(Long id) {
        if (id == null){
            throw new IllegalArgumentException("This bank does not exist");
        }
        return bankRepository.getById(id);
    }

    @Override
    public void delete(Long id) {
        bankRepository.delete(id);
    }

    @Override
    public void update(Long id, Bank newBank) {
        if ((id == null)&&(newBank == null))
            throw new IllegalArgumentException("These parameters can not be null");
        bankRepository.update(id, newBank);
    }

    @Override
    public List<Bank> getAllBanks() {
        return bankRepository.getAllBanks();
    }

    public void setBankRepository(BankRepository bankRepository) {
        this.bankRepository = bankRepository;
    }
}

