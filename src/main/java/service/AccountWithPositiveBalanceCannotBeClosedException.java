package service;

public class AccountWithPositiveBalanceCannotBeClosedException extends RuntimeException {
    public AccountWithPositiveBalanceCannotBeClosedException() {
        super();
    }

    public AccountWithPositiveBalanceCannotBeClosedException(String message) {
        super(message);
    }

    public AccountWithPositiveBalanceCannotBeClosedException(String message, Throwable cause) {
        super(message, cause);
    }

    public AccountWithPositiveBalanceCannotBeClosedException(Throwable cause) {
        super(cause);
    }

    protected AccountWithPositiveBalanceCannotBeClosedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
