package service;

public class TransferringFromClosedAccountIsImpossibleException extends RuntimeException {
    public TransferringFromClosedAccountIsImpossibleException() {
        super();
    }

    public TransferringFromClosedAccountIsImpossibleException(String message) {
        super(message);
    }

    public TransferringFromClosedAccountIsImpossibleException(String message, Throwable cause) {
        super(message, cause);
    }

    public TransferringFromClosedAccountIsImpossibleException(Throwable cause) {
        super(cause);
    }

    protected TransferringFromClosedAccountIsImpossibleException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
