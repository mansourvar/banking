package repository;

import domain.Account;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public interface AccountRepository {
    void save(Account account);

    Account getById(Long id);

    void delete(Long id);

    void update(Long id, Account newAccount);

    List<Account> getAllAccounts();
}
