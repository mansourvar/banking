package repository;

import domain.Bank;

import java.util.List;

public interface BankRepository {
    void save(Bank bank);

    Bank getById(Long id);

    void delete(Long id);

    void update(Long id, Bank newBank);

    List<Bank> getAllBanks();

}
