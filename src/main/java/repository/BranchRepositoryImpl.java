package repository;

import domain.Branch;
import domain.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public class BranchRepositoryImpl implements BranchRepository {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void save(Branch branch) {
        Session session = sessionFactory.getCurrentSession();
        session.save(branch);
    }

    @Transactional(readOnly = true)
    @Override
    public Branch getById(Long id) {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from Branch  where id = :param1", Branch.class)
                .setParameter("param1", id)
                .uniqueResult();
    }

    @Override
    public void delete(Long id) {
        Session session = sessionFactory.getCurrentSession();
        Branch branch = getById(id);
        session.delete(branch);
    }

    @Override
    public void update(Long id, Branch newBranch) {
        Session session = sessionFactory.getCurrentSession();
        Branch branch = getById(id);
        if (newBranch.getAddress() != null) {
            branch.setAddress(newBranch.getAddress());
        }
        if (newBranch.getTelephone() != null) {
            branch.setTelephone(newBranch.getTelephone());
        }
        if (newBranch.getName() != null) {
            branch.setName(newBranch.getName());
        }
        session.update(branch);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Branch> getAllBranches() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from Branch ", Branch.class)
                .setFetchSize(100)
                .list();
    }
}
