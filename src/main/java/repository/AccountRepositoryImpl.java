package repository;

import domain.Account;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public class AccountRepositoryImpl implements AccountRepository {
    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    @Override
    public void save(Account account) {
        Session session = sessionFactory.getCurrentSession();
        session.save(account);
    }

    @Transactional(readOnly = true)
    @Override
    public Account getById(Long id) {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from Account where id = :id", Account.class)
                .setParameter("id", id)
                .uniqueResult();
    }

    @Override
    public void delete(Long id) {
        Session session = sessionFactory.getCurrentSession();
        Account account = getById(id);
        session.delete(account);
    }

    @Override
    public void update(Long id, Account newAccount) {
        Session session = sessionFactory.getCurrentSession();
        Account account = getById(id);
        if (newAccount.getNumber() != null) {
            account.setNumber(newAccount.getNumber());
        }
        if (newAccount.getBalance() != null) {
            account.setCloseDate(newAccount.getCloseDate());
        }
        if (newAccount.getAccountType() != null) {
            account.setBalance(newAccount.getBalance());
        }
        if (newAccount.getOpenDate() != null) {
            account.setAccountType(newAccount.getAccountType());
        }
        if (newAccount.getCloseDate() != null) {
            account.setCloseDate(newAccount.getCloseDate());
        }
        session.update(account);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Account> getAllAccounts() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from Account", Account.class)
                .setFetchSize(100)
                .list();
    }

}
