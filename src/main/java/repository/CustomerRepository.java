package repository;

import domain.Account;
import domain.Customer;

import java.util.List;

public interface CustomerRepository {
    void save(Customer customer);

    Customer getById(Long id);

    Customer getByName(String name);

    void delete(Long id);

    void update(Long id, Customer newCustomer);

    List<Customer> getAllCustomers();

    Customer getByNationalCode(String nationalCode);

    List<Account> getAllAccounts(long id);
}
