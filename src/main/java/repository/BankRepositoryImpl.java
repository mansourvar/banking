package repository;

import domain.Bank;
import domain.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public class BankRepositoryImpl implements BankRepository {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void save(Bank bank) {
        Session session = sessionFactory.getCurrentSession();
        session.save(bank);
    }

    @Transactional(readOnly = true)
    @Override
    public Bank getById(Long id) {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("FROM Bank where id = :param1", Bank.class)
                .setParameter("param1", id)
                .uniqueResult();
    }

    @Override
    public void delete(Long id) {
        Session session = sessionFactory.getCurrentSession();
        Bank bank = getById(id);
        session.delete(bank);
    }

    @Override
    public void update(Long id, Bank newBank) {
        Session session = sessionFactory.getCurrentSession();
        Bank bank = getById(id);
        if (newBank.getDescription() != null) {
            bank.setDescription(newBank.getDescription());
        }
        if (newBank.getName() != null) {
            bank.setName(newBank.getName());
        }
        session.update(bank);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Bank> getAllBanks() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from Bank", Bank.class)
                .setFetchSize(100)
                .list();
    }
}
