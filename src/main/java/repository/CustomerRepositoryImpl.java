package repository;

import domain.Account;
import domain.Customer;
import domain.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;


import java.io.Serializable;
import java.util.List;

@Transactional
public class CustomerRepositoryImpl implements CustomerRepository {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void save(Customer customer) {
        Session session = sessionFactory.getCurrentSession();
        session.save(customer);
    }

    @Transactional(readOnly = true)
    @Override
    public Customer getById(Long id) {
        Session session = sessionFactory.getCurrentSession();
        Customer customer = session.createQuery("from Customer where id = :param1", Customer.class)
                .setParameter("param1", id)
                .uniqueResult();
        return customer;
    }

    @Transactional(readOnly = true)
    @Override
    public Customer getByName(String name) {

        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from Customer where name = :param1", Customer.class)
                .setParameter("param1", name)
                .uniqueResult();
    }

    @Override
    public void delete(Long id) {
        Session session = sessionFactory.getCurrentSession();
        Customer customer = getById(id);
        if (customer == null) {
            return;
        }
        session.delete(customer);
    }
/*
       1- search in db based on id -->   oldCustomer:customer
       2-if first name newCustomer != null
         2-1-set first name oldCustomer
       3-if last name newCustomer !=null
         3-1 set last name oldCustomer
       4-if address newCustomer != null
         4-1 set age oldCustomer
       5-if telephone newCustomer != null
         5-1 set telephone oldCustomer
       6-update oldCustomer
       7-commit
       8-close
    */

    @Override
    public void update(Long id, Customer newCustomer) {
        Session session = sessionFactory.getCurrentSession();
        Customer customer = getById(id);
        if (newCustomer.getFirstName() != null) {

            customer.setFirstName(newCustomer.getFirstName());
        }
        if (newCustomer.getLastName() != null) {

            customer.setLastName(newCustomer.getLastName());
        }
        if (newCustomer.getAddress() != null) {

            customer.setAddress(newCustomer.getAddress());
        }
        if (newCustomer.getTelephone() != null) {

            customer.setTelephone(newCustomer.getTelephone());
        }
        session.update(customer);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Customer> getAllCustomers() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from Customer ", Customer.class)
                .setFetchSize(100)
                .list();
    }

    @Transactional(readOnly = true)
    @Override
    public Customer getByNationalCode(String nationalCode) {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from Customer where nationalCode = :param1", Customer.class)
                .setParameter("param1", nationalCode)
                .uniqueResult();
    }

    @Transactional(readOnly = true)
    @Override
    public List<Account> getAllAccounts(long id) {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from Account ac where ac.customer.id = :id", Account.class)
                .setParameter("id", id)
                .list();
    }
}