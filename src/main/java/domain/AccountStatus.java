package domain;

import java.math.BigDecimal;

public enum AccountStatus {
    ACTIVE(BigDecimal.valueOf(5)),
    CLOSE(BigDecimal.valueOf(0)),
    BLOCK(BigDecimal.valueOf(-1));
    private final BigDecimal limitedBalance;

    AccountStatus (BigDecimal limitedBalance){
        this.limitedBalance = limitedBalance;
    }

    public BigDecimal getLimitedBalance() {
        return limitedBalance;
    }
}
