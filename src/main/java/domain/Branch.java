package domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Branch {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String address;
    private String telephone;
    @OneToOne
    private Bank bank;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Branch branch = (Branch) o;
        return Objects.equals(name, branch.name) &&
                Objects.equals(address, branch.address) &&
                Objects.equals(telephone, branch.telephone) &&
                Objects.equals(bank, branch.bank);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, address, telephone, bank);
    }
}
