package domain;

import java.math.BigDecimal;

public enum AccountType {
    CURRENT_ACCOUNT(BigDecimal.valueOf(20)),
    DEPOSIT_ACCOUNT(BigDecimal.valueOf(20)),
    SAVING_ACCOUNT(BigDecimal.valueOf(10)),
    CREDIT_CARD_ACCOUNT(BigDecimal.valueOf(30));
    private final BigDecimal minBalance;


    AccountType(BigDecimal minBalance)
    {
        this.minBalance = minBalance;
    }
    public BigDecimal getMinBalance() {
        return minBalance;
    }
}
