package configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import repository.*;
import service.AccountServiceImpl;
import service.BankServiceImpl;
import service.BranchServiceImpl;
import service.CustomerServiceImpl;

import javax.sql.DataSource;

@Configuration
public class AccountingConfiguration {
    @Bean
    public AccountRepositoryImpl accountRepository() {
        return new AccountRepositoryImpl();
    }
    @Bean
    public BankRepositoryImpl bankRepository(){
        return new BankRepositoryImpl();
    }
    @Bean
    public BranchRepositoryImpl branchRepository(){
        return new BranchRepositoryImpl();
    }
    @Bean
    public CustomerRepositoryImpl customerRepository(){
        return  new CustomerRepositoryImpl();
    }
    @Bean
    public AccountServiceImpl accountService(AccountRepository accountRepository){
        AccountServiceImpl accountService = new AccountServiceImpl();
        accountService.setAccountRepository(accountRepository);
        return accountService;
    }
    @Bean
    public BankServiceImpl bankService (BankRepository bankRepository){
        BankServiceImpl bankService = new BankServiceImpl();
        bankService.setBankRepository(bankRepository);
        return  bankService;
    }
    @Bean
    public BranchServiceImpl branchService(BranchRepository branchRepository){
        BranchServiceImpl branchService = new BranchServiceImpl();
        branchService.setBranchRepository(branchRepository);
        return  branchService;
    }
    @Bean
    public CustomerServiceImpl customerService (CustomerRepository customerRepository){
        CustomerServiceImpl customerService = new CustomerServiceImpl();
        customerService.setCustomerRepository(customerRepository);
        return customerService;
    }

    @Bean
    public DriverManagerDataSource driverManagerDataSource(){
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        driverManagerDataSource.setUrl("jdbc:mysql://localhost:3306/accounting");
        driverManagerDataSource.setUsername("root");
        driverManagerDataSource.setPassword("root");
        return driverManagerDataSource;
    }
    @Bean
    public LocalSessionFactoryBean sessionFactory(DataSource dataSource){
        LocalSessionFactoryBean localSessionFactoryBean = new LocalSessionFactoryBean();
        localSessionFactoryBean.setDataSource(dataSource);
        return sessionFactory(dataSource);// it is wrong...
    }
}
