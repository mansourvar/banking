import domain.Account;
import domain.Branch;
import domain.Customer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import service.*;

import static domain.AccountStatus.ACTIVE;
import static domain.AccountType.CURRENT_ACCOUNT;
import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@ContextConfiguration("classpath*:spring.xml")
@Transactional
public class AccountServiceTest {
    @Autowired
    private AccountService accountService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private BranchService branchService;


    /*
        1- create an object of Account class -- >account
        2- set the parameters -- > number and balance
        3- save the parameters in accountRepository
        4- getting id's account --> accountId
        5- searching Account in database base on id
        6- after finding put in the local variable
         7- it is expected the number's value of found variable(Account) will be 123
            it is expected the balance's value of found variable(Account) will be 100
         */
    @Test
    public void save() {
        Account account = new Account();
        account.setNumber("123");
        account.setBalance(BigDecimal.valueOf(100));
        account.setAccountType(CURRENT_ACCOUNT);

        Customer customer = new Customer();
        customer.setNationalCode("0000000000");
        customer.setFirstName("Ali");
        customer.setLastName("Mansouri");
        customer.setAddress("Bazaar");
        customer.setTelephone("1234");
        customerService.save(customer);
        Long customerId = customer.getId();
        Customer customerById = customerService.getById(customerId);
        account.setCustomer(customerById);

        Branch branch = new Branch();
        branch.setName("Tajrish");
        branch.setAddress("Tajrish sq");
        branch.setTelephone("321");
        branchService.save(branch);
        Long branchId = branch.getId();
        Branch branchById = branchService.getById(branchId);
        account.setBranch(branchById);

        accountService.save(account);
        Long accountId = account.getId();
        Account accountById = accountService.getById(accountId);

        assertNotNull(accountById);
        assertEquals("123", accountById.getNumber());
        BigDecimal expectedValue = BigDecimal.valueOf(100);
        BigDecimal actualValue = accountById.getBalance();
        assertEquals(0, expectedValue.compareTo(actualValue));
        assertEquals("0000000000", accountById.getCustomer().getNationalCode());
        assertEquals("Ali", accountById.getCustomer().getFirstName());
        assertEquals("Mansouri", accountById.getCustomer().getLastName());
        assertEquals("Bazaar", accountById.getCustomer().getAddress());
        assertEquals("1234", accountById.getCustomer().getTelephone());
        assertEquals("Tajrish", accountById.getBranch().getName());
        assertEquals("Tajrish sq", accountById.getBranch().getAddress());
        assertEquals("321", accountById.getBranch().getTelephone());
    }

    @Test
    public void getById() {
        Customer customer = Customer.createCustomer("0000000000", "Ali", "Mansouri", "Tajrish", "123");
        customerService.save(customer);
        Account account = new Account();
        account.setNumber("321");
        account.setBalance(BigDecimal.valueOf(200));
        account.setCustomer(customer);
        accountService.save(account);
        Long accountId = account.getId();
        Account accountById = accountService.getById(accountId);

        assertNotNull(accountById);
        assertEquals("321", accountById.getNumber());
        BigDecimal expectedValue = BigDecimal.valueOf(200);
        BigDecimal actualValue = accountById.getBalance();
        assertEquals(0, expectedValue.compareTo(actualValue));
    }

    @Test
    public void delete() {
        Customer customer = Customer.createCustomer("0000000000", "Ali", "Mansouri", "Tajrish", "123");
        customerService.save(customer);

        Account account = new Account();
        account.setNumber("321");
        account.setBalance(BigDecimal.valueOf(200));
        account.setCustomer(customer);
        accountService.save(account);
        Long accountId = account.getId();
        accountService.delete(accountId);
        Account accountById = accountService.getById(accountId);

        assertNull(accountById);
    }

    @Test
    public void update() {
        Customer customer = Customer.createCustomer("0000000000", "Ali", "Mansouri", "Tajrish", "123");
        customerService.save(customer);

        Account account = new Account();
        account.setNumber("123");
        account.setBalance(BigDecimal.valueOf(100));
        account.setCustomer(customer);
        accountService.save(account);
        Long accountId = account.getId();

        Account newAccount = new Account();
        newAccount.setNumber("321");


        accountService.update(accountId, newAccount);
        Account accountById = accountService.getById(accountId);

        assertNotNull(accountById);
        assertEquals("321", accountById.getNumber());

        BigDecimal expectedValue = BigDecimal.valueOf(100);
        BigDecimal actualValue = accountById.getBalance();
        assertEquals(0, expectedValue.compareTo(actualValue));
    }

    @Test
    public void getAllAccount() {
        Customer customer = Customer.createCustomer("0000000000", "Ali", "Mansouri", "Tajrish", "123");
        customerService.save(customer);

        Account account = new Account();
        account.setNumber("100");
        account.setCustomer(customer);
        accountService.save(account);

        Customer customer2 = Customer.createCustomer("1111111111", "Ali", "Mansouri", "Tajrish", "123");
        customerService.save(customer2);

        Account account1 = new Account();
        account1.setNumber("200");
        account1.setCustomer(customer2);
        accountService.save(account1);

        Customer customer3 = Customer.createCustomer("2222222222", "Ali", "Mansouri", "Tajrish", "123");
        customerService.save(customer3);

        Account account2 = new Account();
        account2.setNumber("300");
        account2.setCustomer(customer3);
        accountService.save(account2);

        List<Account> allAccounts = accountService.getAllAccounts();

        assertNotNull(allAccounts);
        assertEquals(3, allAccounts.size());
    }

    @Test
    public void getByIdWhenAccountDoesNotExist() {
        Customer customer = Customer.createCustomer("0000000000", "Ali", "Mansouri", "Tajrish", "123");
        customerService.save(customer);

        Account account = new Account();
        account.setBalance(BigDecimal.valueOf(1000));
        account.setNumber("124");
        account.setCustomer(customer);

        Account accountById = accountService.getById(-1L);

        assertNull(accountById);
    }

    @Test(expected = AccountWithoutCustomerException.class)
    public void saveWithoutCustomer() {
        Account account = new Account();
        account.setNumber("200");
        account.setBalance(BigDecimal.valueOf(10000));
        accountService.save(account);
    }

    @Test
    public void withdraw() {
        Account account = new Account();
        account.setNumber("222");
        account.setBalance(BigDecimal.valueOf(100));
        account.setAccountType(CURRENT_ACCOUNT);

        Customer customer = new Customer();
        customer.setNationalCode("2222222222");
        customer.setFirstName("Mary");
        customer.setLastName("Ebr");
        customer.setAddress("Istanbul");
        customer.setTelephone("1234");
        customerService.save(customer);
        Long customerId = customer.getId();
        Customer customerById = customerService.getById(customerId);
        account.setCustomer(customerById);
        accountService.save(account);
        Long accountId = account.getId();

        Account accountById = accountService.getById(accountId);

        accountService.withdraw(accountId, BigDecimal.valueOf(10));

        assertNotNull(accountById);
        assertEquals(BigDecimal.valueOf(90), accountById.getBalance());
    }

    @Test(expected = BalanceIsLessThanThresholdException.class)
    public void balanceIsLessThanRemaining() {
        Account account = new Account();
        account.setNumber("123");
        account.setBalance(BigDecimal.valueOf(100));
        account.setAccountType(CURRENT_ACCOUNT);

        Customer customer = new Customer();
        customer.setNationalCode("1111111111");
        customer.setFirstName("Ali");
        customer.setLastName("Mansouri");
        customer.setAddress("Bazaar");
        customer.setTelephone("1234");
        customerService.save(customer);
        Long customerId = customer.getId();
        Customer customerById = customerService.getById(customerId);
        account.setCustomer(customerById);
        accountService.save(account);
        Long accountId = account.getId();

        accountService.withdraw(accountId, BigDecimal.valueOf(90));
    }

    @Test(expected = BalanceIsLessThanThresholdException.class)
    public void balanceShouldNotComeBelowMinBalanceAfterTransfer() {
        Account sourceAccount = new Account();
        sourceAccount.setNumber("333");
        sourceAccount.setBalance(BigDecimal.valueOf(15000));
        sourceAccount.setAccountType(CURRENT_ACCOUNT);
        sourceAccount.setAccountStatus(ACTIVE);

        Customer sourceCustomer = new Customer();
        sourceCustomer.setFirstName("Mary");
        sourceCustomer.setLastName("Ebr");
        sourceCustomer.setAddress("Istanbul");
        sourceCustomer.setTelephone("11111");
        sourceCustomer.setNationalCode("1111111111");

        customerService.save(sourceCustomer);
        Long sourceCustomerId = sourceCustomer.getId();

        Customer sourceCustomerById = customerService.getById(sourceCustomerId);
        sourceAccount.setCustomer(sourceCustomerById);

        accountService.save(sourceAccount);
        Long sourceAccountId = sourceAccount.getId();

        Account destinationAccount = new Account();
        destinationAccount.setNumber("222");
        destinationAccount.setBalance(BigDecimal.valueOf(1000));
        destinationAccount.setAccountType(CURRENT_ACCOUNT);

        Customer destinationCustomer = new Customer();
        destinationCustomer.setFirstName("Reza");
        destinationCustomer.setLastName("Ebr");
        destinationCustomer.setAddress("Berlin");
        destinationCustomer.setTelephone("0000");
        destinationCustomer.setNationalCode("0000000000");

        customerService.save(destinationCustomer);
        Long destinationCustomerId = destinationCustomer.getId();

        Customer destinationCustomerById = customerService.getById(destinationCustomerId);
        destinationAccount.setCustomer(destinationCustomerById);
        accountService.save(destinationAccount);
        Long destinationAccountId = destinationAccount.getId();

        accountService.transfer(sourceAccountId, destinationAccountId, BigDecimal.valueOf(14990));
    }


    @Test(expected = TransferringToCloseAccountIsImpossibleException.class)
    public void impossibleTransferToClosedAccount() {
        Customer activeCustomer = new Customer();
        activeCustomer.setFirstName("Mary");
        activeCustomer.setLastName("Ebr");
        activeCustomer.setAddress("Istanbul");
        activeCustomer.setTelephone("11111");
        activeCustomer.setNationalCode("1111111111");
        customerService.save(activeCustomer);

        Account activeAccount = new Account();
        activeAccount.setNumber("333");
        activeAccount.setBalance(BigDecimal.valueOf(150000));
        activeAccount.setAccountType(CURRENT_ACCOUNT);
        activeAccount.setCustomer(activeCustomer);
        accountService.save(activeAccount);
        Long activeAccountId = activeAccount.getId();

        Customer nonActiveCustomer = new Customer();
        nonActiveCustomer.setFirstName("Reza");
        nonActiveCustomer.setLastName("Ebr");
        nonActiveCustomer.setAddress("Berlin");
        nonActiveCustomer.setTelephone("0000");
        nonActiveCustomer.setNationalCode("0000000000");
        customerService.save(nonActiveCustomer);

        Account closedAccount = new Account();
        closedAccount.setNumber("222");
        closedAccount.setBalance(BigDecimal.valueOf(0));
        closedAccount.setAccountType(CURRENT_ACCOUNT);
        closedAccount.setCustomer(nonActiveCustomer);
        accountService.save(closedAccount);
        Long closedAccountId = closedAccount.getId();
        accountService.close(closedAccountId);

        accountService.transfer(activeAccountId, closedAccountId, BigDecimal.valueOf(10000));
    }

    @Test(expected = TransferringFromClosedAccountIsImpossibleException.class)
    public void impossibleTransferFromClosedAccountToOtherAccounts() {
        Customer nonActiveCustomer = new Customer();
        nonActiveCustomer.setFirstName("Ali");
        nonActiveCustomer.setLastName("Mansouri");
        nonActiveCustomer.setAddress("Bazaar");
        nonActiveCustomer.setTelephone("123");
        nonActiveCustomer.setNationalCode("1234567890");
        customerService.save(nonActiveCustomer);

        Account closedAccount = new Account();
        closedAccount.setNumber("123");
        closedAccount.setBalance(BigDecimal.valueOf(0));
        closedAccount.setAccountType(CURRENT_ACCOUNT);
        closedAccount.setCloseDate(new Date());
        closedAccount.setCustomer(nonActiveCustomer);
        accountService.save(closedAccount);
        Long closedAccountId = closedAccount.getId();

        Customer activeCustomer = new Customer();
        activeCustomer.setFirstName("Mary");
        activeCustomer.setLastName("Ebr");
        activeCustomer.setAddress("Istanbul");
        activeCustomer.setTelephone("321");
        activeCustomer.setNationalCode("0987654321");
        customerService.save(activeCustomer);

        Account openAccount = new Account();
        openAccount.setNumber("213");
        openAccount.setBalance(BigDecimal.valueOf(20000000));
        openAccount.setAccountType(CURRENT_ACCOUNT);
        openAccount.setCustomer(activeCustomer);
        accountService.save(openAccount);
        Long openAccountId = openAccount.getId();

        accountService.transfer(closedAccountId, openAccountId, BigDecimal.valueOf(2000));
        //Account openAccountById = accountService.getById(openAccountId);

        //assertEquals(BigDecimal.valueOf(20000000),openAccountById.getBalance());
    }


    /*
    1- create a customer --> customer : Customer
    2- create an account --> account : Account
    3- set the parameters of customer --> persist in customerService -->get id
    4- set the parameters of account and set the customer of an account --> persist in accountService -->get id
    5-call close method  in accountService
    6- necessary asserts

     */
    @Test
    public void close() {
        Customer customer = new Customer();
        customer.setNationalCode("1478523690");
        customer.setFirstName("Ali");
        customer.setLastName("Mansouri");
        customer.setAddress("Bazaar");
        customer.setTelephone("1234");
        customerService.save(customer);

        Account account = new Account();
        account.setNumber("000");
        account.setBalance(BigDecimal.valueOf(10000));
        account.setAccountType(CURRENT_ACCOUNT);
        account.setCustomer(customer);
        account.setCloseDate(new Date());
        accountService.save(account);
        Long accountId = account.getId();

        accountService.close(accountId);
        Account accountById = accountService.getById(accountId);

        assertNotNull(accountById.getCloseDate());

        // assertEquals(0.0,accountById.getBalance());
    }

    @Test
    public void deposit() {
        Customer customer = new Customer();
        customer.setFirstName("Ali");
        customer.setLastName("Mansouri");
        customer.setNationalCode("1122334455");
        customerService.save(customer);

        Account account = new Account();
        account.setNumber("098");
        account.setBalance(BigDecimal.valueOf(10000));
        account.setCustomer(customer);
        accountService.save(account);
        Long accountId = account.getId();

        Account accountById = accountService.getById(accountId);

        accountService.deposit(accountId, BigDecimal.valueOf(5000));

        assertNotNull(accountById);
        //  assertEquals(BigDecimal.valueOf(15000.00),accountById.getBalance());

    }
}
