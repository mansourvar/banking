import domain.Account;
import domain.Bank;
import domain.Customer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import service.*;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration("classpath*:spring.xml")
@Transactional
public class BankServiceTest {
    @Autowired
    private BankService bankService;

    @Test
    public void save() {
        Bank bank = new Bank();
        bank.setName("meli");
        bank.setDescription("with high interest");


        bankService.save(bank);
        Long bankId = bank.getId();
        Bank bankById = bankService.getById(bankId);

        assertNotNull(bankById);
        assertEquals("meli", bankById.getName());
        assertEquals("with high interest", bankById.getDescription());
    }

    @Test
    public void getById() {
        Bank bank = new Bank();
        bank.setName("saderat");
        bank.setDescription("with 20 percent interest");
        bankService.save(bank);
        Long bankId = bank.getId();

        Bank bankById = bankService.getById(bankId);

        assertNotNull(bankById);
        assertEquals("saderat", bankById.getName());
        assertEquals("with 20 percent interest", bankById.getDescription());
    }

    @Test
    public void delete() {
        Bank bank = new Bank();
        bank.setName("Tejarat");
        bank.setDescription("with a Lone");
        bankService.save(bank);

        bankService.save(bank);
        Long bankId = bank.getId();
        bankService.delete(bankId);
        Bank bankById = bankService.getById(bankId);

        assertNull(bankById);
    }

    @Test
    public void update() {
        Bank bank = new Bank();
        bank.setName("Melat");
        bank.setDescription("with high interest");
        bankService.save(bank);
        Long bankId = bank.getId();

        Bank newBank = new Bank();
        newBank.setDescription("without high interest");

        bankService.update(bankId, newBank);
        Bank bankById = bankService.getById(bankId);

        assertNotNull(bankById);
        assertEquals("Melat", bankById.getName());
        assertEquals("without high interest", bankById.getDescription());
    }

    @Test
    public void getAllBanks() {
        Bank bank = new Bank();
        bank.setName("Melat");
        bank.setDescription("debit cart included");
        bankService.save(bank);

        Bank bank1 = new Bank();
        bank1.setName("Saderat");
        bank1.setDescription("with interest");
        bankService.save(bank1);

        Bank bank2 = new Bank();
        bank2.setDescription("with daashaagh");
        bankService.save(bank2);

        List<Bank> allBanks = bankService.getAllBanks();

        assertNotNull(allBanks);
        assertEquals(3, allBanks.size());
    }
}
