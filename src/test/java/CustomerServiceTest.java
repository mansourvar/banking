import domain.AccountStatus;
import domain.AccountType;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import domain.Account;
import domain.Customer;
import org.junit.runner.RunWith;
import org.omg.PortableInterceptor.ACTIVE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import service.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static domain.AccountType.CURRENT_ACCOUNT;
import static domain.AccountType.DEPOSIT_ACCOUNT;
import static domain.AccountType.SAVING_ACCOUNT;
import static java.lang.Enum.valueOf;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration("classpath*:spring.xml")
@Transactional
public class CustomerServiceTest {
    @Autowired
    private CustomerService customerService;
    @Autowired
    private AccountService accountService;

    @Test
    public void save() {
        Customer customer = new Customer();
        customer.setFirstName("mary");
        customer.setLastName("ebr");
        customer.setAddress("istanbul");
        customer.setTelephone("12345");
        customer.setNationalCode("0000000000");
        customerService.save(customer);
        Long customerId = customer.getId();

        Customer customerById = customerService.getById(customerId);

        assertNotNull(customerById);
        assertEquals("mary", customerById.getFirstName());
        assertEquals("ebr", customerById.getLastName());
        assertEquals("istanbul", customerById.getAddress());
        assertEquals("12345", customerById.getTelephone());

        BigDecimal expectedValue = BigDecimal.valueOf(1000);
    }

    @Test
    public void getById() {
        Customer customer = new Customer();
        customer.setFirstName("Ali");
        customer.setLastName("mansouri");
        customer.setAddress("tajrish");
        customer.setTelephone("1234");
        customer.setNationalCode("0000000000");
        customerService.save(customer);

        Long customerId = customer.getId();
        Customer customerById = customerService.getById(customerId);

        assertNotNull(customerById);
        assertEquals("Ali", customerById.getFirstName());
        assertEquals("mansouri", customerById.getLastName());
        assertEquals("tajrish", customerById.getAddress());
        assertEquals("1234", customerById.getTelephone());
    }

    @Test
    public void delete() {
        Customer customer = new Customer();
        customer.setFirstName("Ali");
        customer.setLastName("mansouri");
        customer.setAddress("tajrish");
        customer.setTelephone("1234");
        customer.setNationalCode("0000000000");
        customerService.save(customer);

        Long customerId = customer.getId();

        customerService.delete(customerId);
        Customer customerById = customerService.getById(customerId);

        assertNull(customerById);
    }

    @Test
    public void update() {
        Customer customer = new Customer();
        customer.setFirstName("Liam");
        customer.setLastName("Mansourvar");
        customer.setAddress("Istanbul");
        customer.setTelephone("0000");
        customer.setNationalCode("0000000000");
        customerService.save(customer);
        Long customerId = customer.getId();

        Customer newCustomer = new Customer();
        newCustomer.setFirstName("Mary");
        newCustomer.setLastName("Ebr");
        newCustomer.setAddress("Australia");
        newCustomer.setTelephone("12345");

        customerService.update(customerId, newCustomer);
        Customer customerById = customerService.getById(customerId);

        assertNotNull(customerById);
        assertEquals("Mary", customerById.getFirstName());
        assertEquals("Ebr", customerById.getLastName());
        assertEquals("Australia", customerById.getAddress());
        assertEquals("12345", customerById.getTelephone());
    }

    @Test
    public void getAllCustomers() {
        Customer customer = new Customer();
        customer.setFirstName("Liam");
        customer.setAddress("Turkey");
        customer.setNationalCode("0000000000");
        customerService.save(customer);

        Customer customer1 = new Customer();
        customer1.setFirstName("Mary");
        customer1.setAddress("Turkey");
        customer1.setNationalCode("1111111111");
        customerService.save(customer1);

        Customer customer2 = new Customer();
        customer2.setFirstName("Mohsen");
        customer2.setAddress("Turkey");
        customer2.setNationalCode("4444444444");
        customerService.save(customer2);

        List<Customer> allCustomers = customerService.getAllCustomers();

        assertNotNull(allCustomers);
        assertEquals(3, allCustomers.size());
    }

    @Test
    public void getByIdWhenIdIsNonExisted() {
        Customer customerById = customerService.getById(-1L);

        assertNull(customerById);
    }

    @Test
    public void deleteWhenIdIsNonExisted() {
        Customer customer = new Customer();
        customer.setFirstName("Ali");
        customer.setTelephone("12345");
        customer.setNationalCode("1111111111");
        customerService.save(customer);

        customerService.delete(-1L);
        List<Customer> allCustomers = customerService.getAllCustomers();

        assertEquals(1, allCustomers.size());
    }

    @Test
    public void updateWhenIdIsNonExisted() {
        Customer customer = new Customer();
        customer.setFirstName("Mohsen");
        customer.setLastName("Mansouri");
        customer.setAddress("Istanbul");
        customer.setTelephone("123");
        customer.setNationalCode("3333333333");
        customerService.save(customer);
        Long customerId = customer.getId();

        Customer newCustomer = new Customer();
        newCustomer.setAddress("Australia");

        customerService.update(customerId, newCustomer);
        Customer customerById = customerService.getById(customerId);

        assertEquals("Mohsen", customerById.getFirstName());
        assertEquals("Mansouri", customerById.getLastName());
        assertEquals("Australia", customerById.getAddress());
        assertEquals("123", customerById.getTelephone());
    }

    @Test
    public void getAllCustomerWhenDataIsEmpty() {
        List<Customer> allCustomers = customerService.getAllCustomers();

        assertEquals(0, allCustomers.size());
        assertTrue(allCustomers.isEmpty());
    }

    @Test
    public void getByNationalCode() {
        Customer customer = new Customer();
        customer.setFirstName("Ali");
        customer.setLastName("Mansouri");
        customer.setAddress("Bazaar");
        customer.setTelephone("12345");
        customer.setNationalCode("0000000000");
        customerService.save(customer);
        String nationalCode = customer.getNationalCode();

        Customer byNationalCode = customerService.getByNationalCode(nationalCode);

        assertNotNull(byNationalCode);
        assertEquals(10, byNationalCode.getNationalCode().length());
        assertEquals("Ali", byNationalCode.getFirstName());
        assertEquals("Mansouri", byNationalCode.getLastName());
        assertEquals("Bazaar", byNationalCode.getAddress());
        assertEquals("12345", byNationalCode.getTelephone());
        assertEquals("0000000000", byNationalCode.getNationalCode());
    }

    @Test(expected = CustomerWithSameNationalCodeExistedException.class)
    public void saveCustomerWithSameNationalCode() {
        Customer customer1 = new Customer();
        customer1.setFirstName("mary");
        customer1.setLastName("Ebr");
        customer1.setNationalCode("1111111111");
        customerService.save(customer1);

        Customer customer2 = new Customer();
        customer2.setFirstName("Liam");
        customer2.setLastName("Mansouri");
        customer2.setNationalCode("1111111111");
        customerService.save(customer2);
    }

    @Test
    public void findAllCustomerAccounts() {
        Customer customer1 = new Customer();
        customer1.setFirstName("Mary");
        customer1.setLastName("Ebr");
        customer1.setNationalCode("5555555555");
        customerService.save(customer1);

        Account account1 = new Account();
        account1.setNumber("123");
        account1.setAccountType(CURRENT_ACCOUNT);
        account1.setBalance(BigDecimal.valueOf(2000000));
        account1.setCustomer(customer1);
        accountService.save(account1);


        Account account2 = new Account();
        account2.setNumber("321");
        account2.setAccountType(DEPOSIT_ACCOUNT);
        account2.setBalance(BigDecimal.valueOf(50000000));
        account2.setCustomer(customer1);
        accountService.save(account2);
        Long customer1Id = customer1.getId();


        Customer customer2 = new Customer();
        customer2.setFirstName("Liam");
        customer2.setLastName("Mansouri");
        customer2.setNationalCode("7777777777");
        customerService.save(customer2);

        Account account3 = new Account();
        account3.setNumber("111");
        account3.setBalance(BigDecimal.valueOf(12000));
        account3.setAccountType(CURRENT_ACCOUNT);
        account3.setCustomer(customer2);
        accountService.save(account3);

        List<Account> allAccounts = customerService.getAllAccounts(customer1Id);
        List<Account> customer2AllAccounts = customerService.getAllAccounts(customer2.getId());

        assertNotNull(allAccounts);
        assertEquals(2, allAccounts.size());
        assertTrue(allAccounts.containsAll(Arrays.asList(account1, account2)));
        //assertThat(allAccounts, CoreMatchers.everyItem(account1,account2));
        /*List<Account> expectedAccounts = new ArrayList<>();
        expectedAccounts.add(account1);
        expectedAccounts.add(account2);*/
        assertNotNull(customer2AllAccounts);
        assertEquals(1, customer2AllAccounts.size());
        assertEquals(Collections.singletonList(account3), customer2AllAccounts);



        /*Account actualAccount1 = allAccounts.get(0);
        //account1.equals(actualAccount1);
        assertEquals(account1,actualAccount1);
        Account actualAccount2 = allAccounts.get(1);
        assertEquals(account2,actualAccount2);*/


        /*Account a1 = allAccounts.get(0);
        assertEquals(account1.getNumber(),a1.getNumber());
        account1.getNumber().equals(a1.getNumber());
        assertEquals(account1.getBalance(),a1.getBalance());
        assertEquals(account1.getAccountType(),a1.getAccountType());*/
    }

    @Test
    public void findSumTotalBalance() {
        Customer customer = Customer.createCustomer("0000000000", "Ali", "Mansouri", "Tajrish", "123");
        customerService.save(customer);
        Long customerId = customer.getId();

        Account account1 = new Account();
        account1.setNumber("111");
        account1.setAccountType(CURRENT_ACCOUNT);
        account1.setBalance(BigDecimal.valueOf(12000));
        account1.setCustomer(customer);
        accountService.save(account1);

        Account account2 = new Account();
        account2.setNumber("222");
        account2.setAccountType(CURRENT_ACCOUNT);
        account2.setBalance(BigDecimal.valueOf(15000));
        account2.setCustomer(customer);
        accountService.save(account2);

        BigDecimal sumTotalBalance = customerService.findSumTotalBalance(customerId);

        assertNotNull(sumTotalBalance);
        assertEquals(BigDecimal.valueOf(2700),sumTotalBalance);
    }
}
