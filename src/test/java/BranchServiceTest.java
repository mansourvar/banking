import domain.Bank;
import domain.Branch;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import service.BankService;
import service.BankServiceImpl;
import service.BranchService;
import service.BranchServiceImpl;
@RunWith(SpringRunner.class)
@ContextConfiguration("classpath*:spring.xml")
@Transactional
public class BranchServiceTest {
    @Autowired
    private BranchService branchService;
    @Autowired
    private BankService bankService ;

    @Test
    public void save() {
        Branch branch = new Branch();
        branch.setName("Tajrish");
        branch.setAddress("Tajrish sq");
        branch.setTelephone("12345");
        branchService.save(branch);

        Bank bank = new Bank();
        bank.setName("Melat");
        bank.setDescription("with high interest");
        bankService.save(bank);
        Long bankId = bank.getId();
        Bank bankById = bankService.getById(bankId);

        branch.setBank(bankById);
        Long branchId = branch.getId();
        Branch branchById = branchService.getById(branchId);

        assertNotNull(branchById);
        assertEquals("Tajrish", branchById.getName());
        assertEquals("Tajrish sq", branchById.getAddress());
        assertEquals("12345", branchById.getTelephone());
        assertEquals("Melat", bankById.getName());
        assertEquals("with high interest", bankById.getDescription());
    }

    @Test
    public void getById() {
        Branch branch = new Branch();
        branch.setName("Tajrish");
        branch.setAddress("Tajrish sq");
        branch.setTelephone("12345");
        branchService.save(branch);
        Long branchId = branch.getId();

        Branch branchById = branchService.getById(branchId);

        assertNotNull(branchById);
        assertEquals("Tajrish", branchById.getName());
        assertEquals("Tajrish sq", branchById.getAddress());
        assertEquals("12345", branchById.getTelephone());

    }

    @Test
    public void delete() {
        Branch branch = new Branch();
        branch.setName("Tajrish");
        branch.setAddress("Tajrish sq");
        branch.setTelephone("12345");
        branchService.save(branch);
        Long branchId = branch.getId();

        branchService.delete(branchId);
        Branch branchById = branchService.getById(branchId);

        assertNull(branchById);
    }
    @Test
    public void update(){
        Branch branch = new Branch();
        branch.setName("Tajrish");
        branch.setAddress("Tajrish sq");
        branch.setTelephone("12345");
        branchService.save(branch);
        Long branchId = branch.getId();

        Branch newBranch = new Branch();
        newBranch.setAddress("Vanak");
        newBranch.setTelephone("54321");

        branchService.update(branchId,newBranch);
        Branch branchById = branchService.getById(branchId);

        assertNotNull(branchById);
        assertEquals("Tajrish",branchById.getName());
        assertEquals("Vanak",branchById.getAddress());
        assertEquals("54321",branchById.getTelephone());

    }
}
